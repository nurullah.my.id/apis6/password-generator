package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
)

func homePage(w http.ResponseWriter, r *http.Request) {
	// Also update version over here
	fmt.Fprintf(w, "Password Generator v1.0.13")
	fmt.Println("Endpoint Hit: homePage")
}

func ping(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Pong!")
	fmt.Println("Endpoint Hit: ping")
}

func init() {
	rand.Seed(time.Now().UnixNano())
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func randStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func weak(w http.ResponseWriter, r *http.Request) {
	var pass = randStringRunes(8)
	json.NewEncoder(w).Encode(pass)
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

func handleRequests() {
	myRouter := mux.NewRouter().StrictSlash(true)

	myRouter.HandleFunc("/", homePage).Methods("GET")
	myRouter.HandleFunc("/ping", ping).Methods("GET")
	myRouter.HandleFunc("/weak", weak).Methods("GET")

	log.Fatal(http.ListenAndServe(":10000", myRouter))
}

func loadEnv(dotenv string) {
	err := godotenv.Load(dotenv)
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}

func main() {
	// Command flag
	wordPtr := flag.String("env", "development", "Load Environment Variables")
	flag.Parse()

	if *wordPtr == "development" {
		fmt.Println("Loading Environment: DEVELOPMENT")
		loadEnv("config/dev.env")
	} else if *wordPtr == "stage" {
		fmt.Println("Loading Environment: STAGE")
		loadEnv("config/stage.env")
	} else if *wordPtr == "production" {
		fmt.Println("Loading Environment: PRODUCTION")
		loadEnv("config/prod.env")
	}

	// Say Hello, print the environment, and version
	fmt.Println("Rest API v2.0 - Mux Routers. Project: Password Generator")
	fmt.Println("Environment", os.Getenv("ENVIRONMENT"))
	fmt.Println("Version", os.Getenv("VERSION"))

	// Debug environment variable over here
	var dbeng = os.Getenv("DB_ENGINE")
	var dbhost = os.Getenv("DB_HOST")
	var dbport = os.Getenv("DB_PORT")
	var dbname = os.Getenv("DB_NAME")
	var dbschema = os.Getenv("DB_SCHEMA")
	var dbuser = os.Getenv("DB_USER")
	var dbpass = os.Getenv("DB_PASSWORD")

	println("ENVIRONMENT:", dbeng, dbhost, dbport, dbname, dbschema, dbuser, dbpass)

	handleRequests()
}
